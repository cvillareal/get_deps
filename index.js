const simple_deps = {
  A: ["D", "E"],
};

const normal_deps = {
  A: ["D", "B"],
  B: ["C", "E"],
  C: ["D", "E"],
};

const single_deps = {
  A: ["D", "B"],
  B: ["C", "E"],
  C: ["D", "E"],
  D: ["F"],
};

const cyclical_deps = {
  A: ["D", "B"],
  B: ["C", "E"],
  C: ["D", "E"],
  D: ["F"],
  F: ["A"],
};

const recursively_get_deps = (parent, path, max_depths, dependency_map) => {
  // check for cyclical dependency
  if (path.includes(parent)) {
    throw new Error(
      `Cyclical dependency - ${parent} is already in ${JSON.stringify(path)}`
    );
  }

  path = path.concat(parent);

  // create new depth entry, update if higher depth
  if (!max_depths[parent] || max_depths[parent] < path.length) {
    max_depths[parent] = path.length;
  }

  const children = dependency_map[parent];

  if (!children) {
    return;
  }

  children.forEach((child) => {
    recursively_get_deps(child, path, max_depths, dependency_map);
  });
};

// returns install order by deepest depth first
const sort_by_depth = (max_depths) => {
  return Object.entries(max_depths)
    .sort((a, b) => b[1] - a[1])
    .map((pair) => pair[0]);
};

const get_deps = (package, dependency_map) => {
  const max_depths = {};

  recursively_get_deps(package, [], max_depths, dependency_map);

  const install_order = sort_by_depth(max_depths);

  return install_order;
};

console.log("----\n", get_deps("A", simple_deps)); // [D, E, A]
console.log("----\n", get_deps("A", normal_deps)); // [D, E, C, B, A]
console.log("----\n", get_deps("A", single_deps)); // [F, D, E, C, B, A]
console.log("----\n", get_deps("A", cyclical_deps)); // Error

//   # circular dependency, throw exception
//   # remove duplicates
//   # installation order = array order
